#-*- coding:utf-8 -*-
'''
Created on 27.04.2018 г.

@author: dedal
'''
import socket
import json
import time
import threading
import SocketServer
import sysobj
import event
import log

class EchoRequestHandler(SocketServer.BaseRequestHandler):
    
    def handle(self, udp_buffer=sysobj.UDP_BUFFER):  # @UndefinedVariable @ReservedAssignment
#         MEM_CASH = db.MemDB()
        data = self.request[0] 
        var = ''
        count = 1
        while True:
            if len(data)< udp_buffer:
                break
            elif data[-4:] == 'EXIT':
                break
            var = self.request[1].recv(udp_buffer)
            data = data + var
            count += 1
        if count > 1:
            data = data.replace('EXIT', '')
        data = sysobj.UDP_CRYPT.decrypt(data)
        log.stdout_udp.debug('SERVER REQUEST: %s' % (data))
        try:
            data = json.loads(data)
        except ValueError:
            data = sysobj.UDP_CRYPT.encrypt(json.dumps(None))
            self.request[1].sendto(data , self.client_address)
            return None
        if data[0][0:4] == 'sas.':
            sysobj.SAS_REQUEST_Q.put(data)
            try:
                data = sysobj.SAS_RESULT_Q.get(timeout=sysobj.UDP_TIMEOUT - 1)
            except sysobj.Empty:
                data = None
        else:
            data = event.EVENT[data[0]](**data[1])
        log.stdout_udp.debug('SERVER RESPONSE: %s' % (data))
        data = sysobj.UDP_CRYPT.encrypt(json.dumps(data))
        start = 0
        end = udp_buffer
        count = 1
        while True:
            if len(data[start:]) < udp_buffer:
                self.request[1].sendto(data[start:] , self.client_address)
                break
            elif len(data[end:]) == 0:
                break
            else:
                self.request[1].sendto(data[start:end], self.client_address)
                
            count += 1
            start = end
            end += udp_buffer
            time.sleep(sysobj.UDP_RESPONSE_SLEEP)
        
        if count > 1:
#             time.sleep(0.2)
            self.request[1].sendto('EXIT', self.client_address)
        return True


    
class Client():

    def __init__(self, ip, timeout, port, udp_buffer):
        self.udp_buffer = udp_buffer
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.s.settimeout(timeout)  # @UndefinedVariable
        self.ip = ip
        self.port = port
    
    def send(self, msg, **kwargs):
        msg = msg, kwargs
        msg = json.dumps(msg)
        log.stdout_udp.debug('CLIENT REQUEST: %s' % (msg))
        data = sysobj.UDP_CRYPT.encrypt(msg)
        start = 0
        end = self.udp_buffer
        count = 1
        while True:
            if len(data[start:]) < self.udp_buffer and len(data[start:]) != 0:
                self.s.sendto(data[start:], (self.ip, self.port))
                break
            elif len(data[start:]) == 0:
                break
            else:
                self.s.sendto(data[start:end], (self.ip, self.port))
            start = end
            end += self.udp_buffer
            count += 1
            time.sleep(sysobj.UDP_RESPONSE_SLEEP)
        
        if count > 1:
            self.s.sendto('EXIT', (self.ip, self.port))
        data = ''
        count = 1
        while True:
            data = data + self.s.recv(self.udp_buffer)
            if len(data) < self.udp_buffer:
                break
            elif data[-4:] == 'EXIT':
                break
            count += 1
        if count > 1:
            data = data.replace('EXIT', '')

        data = sysobj.UDP_CRYPT.decrypt(data)
        log.stdout_udp.debug('CLIENT RESPONSE: %s' % (data))
        try:
            data = json.loads(data)
        except ValueError:
            return None
        return data
    
    def close(self):
        self.s.close()
        return True


def send(evt, jp_ip=sysobj.JPServerIP, port=sysobj.JPServerPort, udp_buffer=sysobj.UDP_BUFFER, timeout=sysobj.UDP_TIMEOUT, **kwargs):  
    '''
    Изпраща информация към TCP сървър.
    _tcp.Client
    :param evt: Име на функция на отдалечения сървър.
    :param kwargs: Аргументи ако има
    :return: Връща отговора на функцията. Ако е неуспешно None
    '''
    try:
        log.stdout_udp.debug('SEND COMMAND: %s' % (evt))
        client = Client(ip=jp_ip, port=port, timeout=timeout, udp_buffer=udp_buffer)  
        response = client.send(evt, **kwargs)
        client.close()
    except socket.error as e:
        log.stdout_udp.warn('SEND ERROR: %s, evt: %s' % (e, evt))
        response = None
    return response

def run_server(port=sysobj.UDP_PORT, *args):  # @UndefinedVariable
    '''
    Стартита TCP сървър като демон.
    Използва _tcp.EchoRequestHandler
    :param port:  Порт на лоцалния TCP сървър. Взима се от conf
    :param args: Ne se podawat argumenti
    :return: Не връща резултат. Стартира безкраен демон.
    '''
    log.stdout_udp.info('SERVER PROC STARTING!')
    address = ('', port) # let the kernel give us a port
    server = SocketServer.UDPServer(address, EchoRequestHandler)
    #server._handle_request_noblock()
    server.allow_reuse_address = True
    server.allow_reuse = True
    # ip, port = server.server_address # find out what port we were given
    t = threading.Thread(target=server.serve_forever())
    t.setDaemon(True) # don't hang on exit
    t.start()