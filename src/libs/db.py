#-*- coding:utf-8 -*-
'''
Created on 27.04.2018 г.

@author: dedal
'''
import datetime
import json 
import socket
import os
from pymemcache.client.base import Client as mem_Client
from pymemcache.exceptions import MemcacheUnexpectedCloseError
from memcached_stats import MemcachedStats
import sqlite3
import bsddb3
import MySQLdb
import psycopg2

# KEYS = None
class MemDBCloseWarning(MemcacheUnexpectedCloseError):
    pass

class MemDBSocketError(socket.error):
    pass

class DBBadDump(Exception):
    pass

class DBBadVersion(Exception):
    pass

class BadSerializationFormat(Exception):
    pass

class DBValueError(Exception):
    pass

class NoSQLDBConnection(Exception):
    pass

class NoSQLiteDB(Exception):
    pass




class Berkeley():
 
    def __init__(self, name, path=None, crypt=None):  # @UndefinedVariable
        self.crypt = crypt
        self.db = bsddb3.db.DB()
        if path == None:
            self.db.open(name, None, bsddb3.db.DB_HASH, bsddb3.db.DB_CREATE)
        else:
            self.db.open(path + name, None, bsddb3.db.DB_HASH, bsddb3.db.DB_CREATE)
 
    def close(self):
        self.db.close()
        return True
 
    def sync(self):
        self.db.sync()
        return True
 
    def set_key(self, key, data=None):
        if data == None:
            data = {}
        try:
            if self.crypt != None:
                self.db[key] = self.crypt.encrypt(json.dumps(data))
            else:
                self.db[key] = json.dumps(data)
        except ValueError as e:
            raise DBValueError, e
        return True
 
    def get_key(self, key):
        try:
            key = self.db[key]
            if self.crypt != None:
                return json.loads(self.crypt.decrypt(key))
            else:
                return json.loads(key)
        except ValueError as e:
            raise DBValueError, e
 
    def set_key_to(self, to_key, key, data):
        try:
            if self.crypt != None:
                from_db = json.loads(self.crypt.decrypt(self.db[to_key]))
                from_db[key] = data
                self.db[to_key] = self.crypt.encrypt(json.dumps(from_db))
            else:
                from_db = json.loads(self.db[to_key])
                from_db[key] = data
                self.db[to_key] = json.dumps(from_db)
            return True
        except ValueError as e:
            raise DBValueError, e
 
    def keys(self):
        try:
            data = self.db.keys()
            var = []
            for item in data:
                var.append(item)
            return var
        except ValueError as e:
            raise DBValueError, e
 
    def keys_from(self, key_from):
        try:
            if self.crypt != None:
                key = self.crypt.encrypt(key_from)
                data = json.loads(self.crypt.decrypt(self.db[key],))
            else:
                key = key_from
                data = json.loads(self.db[key],)
            return data.keys()
        except ValueError as e:
            raise DBValueError, e
 
    def dell(self, key, del_key):
        try:
            db_key = self.get_key(key)
            del db_key[del_key]
            self.set_key(key, db_key)
            return True
        except ValueError as e:
            raise DBValueError, e

def json_serializer(key, value):
    if type(value) == str:
        return value, 1
    return json.dumps(value), 2

def json_deserializer(key, value, flags):
    if flags == 1:
        return value
    if flags == 2:
        return json.loads(value)
    raise BadSerializationFormat, "Unknown serialization format"

class MemDB():
    def __init__(self, host, port, dump_db=None):
        
#         self.db = Berkeley()
        self.dump = dump_db
        self.mem_cach = mem_Client((host, port), serializer=json_serializer, deserializer=json_deserializer)
        if self.dump  != None:
            self.all_keys = self.dump.keys()
#             KEYS = self.all_keys
            for i in self.all_keys:
                self.dump.get_key(i)
                self.mem_cach.set(i, self.dump.get_key(i))
        else:
            mem = MemcachedStats(host, port)
            self.all_keys = mem.keys()
        
                
    def get_key(self, key):
        try:
            return self.mem_cach.get(key)
        except socket.error:
            raise MemDBSocketError
        except MemcacheUnexpectedCloseError:
            raise MemDBCloseWarning
    
    def set_key(self, key, data=None):
        if data == None:
            data = {}
        try:
            self.mem_cach.set(key, data)
        except socket.error:
            raise MemDBSocketError
        except MemcacheUnexpectedCloseError:
            raise MemDBCloseWarning
        return True
    
    def set_key_to(self, to_key, key, data):
        try:
            db_data = self.get_key(to_key)
            db_data[key] = data
            self.set_key(to_key, db_data)
            return True 
        except socket.error:
            raise MemDBSocketError
        except MemcacheUnexpectedCloseError:
            raise MemDBCloseWarning
        
    def keys(self):
        try:
            return self.all_keys
        except socket.error:
            raise MemDBSocketError
        except MemcacheUnexpectedCloseError:
            raise MemDBCloseWarning
    
    def keys_from(self, key_from):
        try:
            return self.mem_cach.get(key_from).keys()
        except socket.error:
            raise MemDBSocketError
        except MemcacheUnexpectedCloseError:
            raise MemDBCloseWarning
    
    def dell(self, key, del_key):
        try:
            db_data = self.get_key(key)
            del db_data[del_key]
            self.set_key(key, db_data)
            return True
        except socket.error:
            raise MemDBSocketError
        except MemcacheUnexpectedCloseError:
            raise MemDBCloseWarning
    
    def mk_dump(self):
        try:
            if self.dump != None:
                keys = self.keys()
                for i in keys:
                    data = self.mem_cach.get(i)
                    self.dump.set_key(i, data)
                self.dump.sync()
            return True
        except socket.error:
            raise MemDBSocketError
        except MemcacheUnexpectedCloseError:
            raise MemDBCloseWarning
        return None
    
    def sync(self, key):
        try:
            if self.dump != None:
                data = self.mem_cach.get(key)
                self.dump.set_key(key, data)
                self.dump.sync()
            return True
        except socket.error:
            raise MemDBSocketError
        except MemcacheUnexpectedCloseError:
            raise MemDBCloseWarning
        return None

class SQLdb():
    
    def __init__(self, dbname, user, passwd, host, port):
        '''
        Параметри на инстанцията:
        self.dbname
        self.user
        self.passwd
        self.conn
        self.db
        '''
        
        self.dbname = dbname
        self.user = user
        self.passwd = passwd
        if host == 'localhost': 
            host = '127.0.0.1'
        self.host = host
        self.port = port
        
    def get(self, query):
        '''  
        Използване:
            db = MySQL('testing', 'root', 'my_passwd', 'localhost')
            a = db.get('select * from user')
            print a['Name']
        
        Връща:
            Първия резултат от заявката като речник.
        '''
        self.db.execute(query)
        result = self.db.fetchone()
        return result
    
    def get_all(self, query):
        '''
        Използване:
            db = MySQL('testing', 'root', 'my_passwd', 'localhost')
            a = db.get_all('select * from user')
            print a[1]['Name']
            
        Връща:
            Резултата от заявката като наредена точка от речници.
        '''
        self.db.execute(query)
        result = self.db.fetchall()
        return result
    
    def set(self, query):
        '''
        Използване:
            db = MySQL('testing', 'root', 'my_passwd', 'localhost')
            a = db.set("INSERT INTO user(`Name`, `E-mail`, `sity`, `adres`) VALUES ('Гергана Дикиджиева','geci83@abv.bg','Тополи','ул.Овчага 71')")
            print a
            
        Връща:
            True
        '''
        self.db.execute(query)
        return True
    
    def set_many(self, query, value):
        '''
        Използване:
            db = MySQL('testing', 'root', 'my_passwd', 'localhost')
            a = db.set_many("""INSERT INTO user(`Name`, `E-mail`, `sity`, `adres`) 
                            VALUES (%s, %s, %s, %s)""",
                            [
                                ('1','1','1','1'),
                                ('2','2','2','2')
                            ]
                        )
        
        Връща:
            True
        '''
        self.db.executemany(query, value)
        return True

    def close(self):
        '''
        Използване:
            db = MySQL('testing', 'root', 'my_passwd', 'localhost')
            db.close()
            
        Връща:
            True
        '''
        self.conn.close()
        return True
    
    def commit(self):
        self.conn.commit()
    
class MySQL(SQLdb):
    '''
    Работи с MySQL база данни:
    
    Функции:
    __init__(dbname, user, passwd, host)
    get(query)
    get_all(query)
    set(query)
    set_many(query, value)
    close()
    _backup(path)
    _restory(scrypt)
    '''

    def connect(self):
        try:
            self.conn = MySQLdb.connect(db=self.dbname, user=self.user, host=self.host, passwd=self.passwd)
            self.conn.set_character_set('utf8')
            self.db = self.conn.cursor()
        except Exception as e:
            raise NoSQLDBConnection, e
    
    
        
    def _backup(self, path, dump=None):
        '''
        Важно:
            Нуждае се от mysqldump.exe!
        
        Използване:
            db = MySQL('testing', 'root', 'my_passwd', 'localhost')
            a = db._backup('d:/Python/coffee-trade/bin/lib/')
            print a
            
        Връща:
            True
        '''
        path = path + '/' + self.dbname + '_' + 'bakup'
        now = datetime.datetime.now()
        now = now.strftime(path + '_' + "%d-%m-%Y_%H-%M-%S") + '.sql'
        if dump == None:
            command = '''mysqldump -u %s --password="%s" %s > %s''' % (self.user, self.passwd, self.dbname, now )
        else: 
            command = '''%s -u %s --password="%s" %s > %s''' % (dump, self.user, self.passwd, self.dbname, now )
        os.popen(command)
        self.close()
        return True
    
    def _restore(self, scrypt):
        '''
        Използване:
            db = MySQL('testing', 'root', 'my_passwd', 'localhost')
            a = db._restory('testing_bakup_21-12-2014_16-38-29.sql')
            print a
            
        Връща:
            True
        '''
        data = open(scrypt, 'r')
        query = " ".join(data.readlines())
        self.db.execute(query)
        return True
    
class SQLite(SQLdb):
    '''
    Работи с SQLite база данни:
    
    Функции:
        __init__(dbname)
        get(query)
        get_all(query)
        set(query)
        set_many(query, value)
        close()
        _backup(path)
        _restory(scrypt)
    '''
    
    def __init__(self, dbname):
        '''
        Параметри на инстанцията:
        self.dbname
        self.conn
        self.db
        '''
        
        self.dbname = dbname
        try:
            open(self.dbname, 'r').close()
        except Exception as e:
            raise NoSQLiteDB, e
        self.conn = sqlite3.connect(self.dbname)  # @UndefinedVariable
        self.conn.row_factory = sqlite3.Row  # @UndefinedVariable
        self.conn.text_factory = str
        
    def connect(self):
        try:
            self.db = self.conn.cursor()
            self.db.execute('PRAGMA encoding="UTF-8";')
        except Exception as e:
            raise NoSQLDBConnection, e
    
    def _backup(self, path):
        '''
        Използване:
            db = MySQL('testing', 'root', 'my_passwd', 'localhost')
            a = db._backup('d:/Python/coffee-trade/bin/lib/')
            print a
            
        Връща:
            True
        '''
        path = path + '/' + self.dbname + '_' + 'bakup'
        now = datetime.datetime.now()
        now = now.strftime(path + '_' + "%d-%m-%Y_%H-%M-%S") + '.sql'
        with open( now, 'w') as f:
            for line in self.conn.iterdump():
                f.write('%s\n' % line)
        f.close()
        self.commit()
        return True

    def _restore(self, scrypt):
        '''
        Използване:
            db = MySQL('testing', 'root', 'my_passwd', 'localhost')
            a = db._restory('testing_bakup_21-12-2014_16-38-29.sql')
            print a
            
        Връща:
            True
        '''
        alltable = self.get_all('select * from sqlite_sequence')
        for i in alltable:
            query = 'drop table %s' % ( i['name'] )
            self.set(query)
        scrypt = file(scrypt, 'r').read()
        self.db.executescript(scrypt)
        self.commit()
        return True
    
class PostgreSQL(SQLdb):
        
    def connect(self):
        try:
            self.conn = psycopg2.connect(dbname=self.dbname, user=self.user, host=self.host, password=self.passwd, port=self.port)
        except Exception as e:
            raise NoSQLDBConnection, e
        self.db = self.conn.cursor()
        
    def _backup(self, backup_path, table_names=None):
        
        now = datetime.datetime.now()
        filename = now.strftime(self.dbname + '_' + "%d-%m-%Y_%H-%M-%S") + '.backup'
        
        command_str = str(self.host)+" -p "+str(self.port)+" -d "+self.dbname+" -U "+self.user
        command_str = "pg_dump -h "+command_str
        
        if table_names is not None:
            for x in table_names:
                command_str = command_str +" -t "+x
        
        command_str = command_str + " -F c -b -v -f '"+backup_path+"/"+filename+"'"
        try:
            os.system(command_str)
            print "Backup completed"
        except Exception as e:
            print "!!Problem occured!!"
            print e

    def _restore(self, backup_path, filename, table_names=None):
        command_str = str(self.host)+" -p "+str(self.port)+" -d "+self.dbname+" -U "+self.user
        command_str = "pg_restore -h "+command_str
        
        if table_names is not None:
            for x in table_names:
                command_str = command_str +" -t "+x
        
        command_str = command_str + " -v '"+backup_path+"/"+filename+"'"
        try:
            os.system(command_str)
            print "Restore completed"
        except Exception as e:
            print "!!Problem occured!!"
            print e



if __name__ == '__main__':
    DB = Berkeley('my_db')
    DB.set_key('my_key', data={1: 'data 1', 2: True, 3:{1:'data'})
    DB.sync()
    MEM_DB = MemDB(host='127.0.0.1', port=11211, dump_db=DB)
    MEM_DB.get_key('my_key')