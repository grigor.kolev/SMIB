#-*- coding:utf-8 -*-
'''
Created on 20.02.2018 г.

@author: dedal
'''
from Crypto import Random
from Crypto.Cipher import AES
import shelve
import os
import base64


class Crypt():
    def __init__(self, key, iv, iv_jump=False):
        self.key = key
        self.iv = iv
        self.bs = 16
        self.iv_jump = iv_jump

    def encrypt(self, my_message):
        if self.iv_jump == False:
            obj = AES.new(self.key, AES.MODE_CFB, self.iv)
        else:
            raw = self._pad(my_message)
            iv = Random.new().read(AES.block_size)
            cipher = AES.new(self.key, AES.MODE_CBC, iv)
            return base64.urlsafe_b64encode(iv + cipher.encrypt(raw)) 
        return obj.encrypt(my_message)

    def decrypt(self, my_message):
        if self.iv_jump == False:
            obj2 = AES.new(self.key, AES.MODE_CFB, self.iv)
        else:
            enc = base64.urlsafe_b64decode(my_message.encode('utf-8'))
            iv = enc[:self.bs]
            cipher = AES.new(self.key, AES.MODE_CBC, iv)
            return self._unpad(cipher.decrypt(enc[self.bs:]))
        return obj2.decrypt(my_message)
    
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)
    
    def _unpad(self, s):
        return s[:-ord(s[len(s)-1:])]
    
    @staticmethod        
    def key_maker(key_len=32):
        return os.urandom(key_len)

    @staticmethod 
    def vector_maker():
        return Random.new().read(AES.block_size)
    
    @staticmethod 
    def holder_maker(keys, path, system_name):
        if system_name == 'posix':
            holder = shelve.open(path + 'libholder.so', 'c')
        else:
            holder = shelve.open(path + 'libholder.dll', 'c')
        holder['key'] = keys
        holder.close()
        return True
    
    @staticmethod 
    def new_holder():
        return base64.b64encode(
                         Crypt.key_maker() +  Crypt.key_maker() +
                         Crypt.key_maker() + Crypt.key_maker() +
                         Crypt.key_maker() + Crypt.key_maker()+
                         Crypt.key_maker() + Crypt.vector_maker()
                         )
