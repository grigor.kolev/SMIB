#-*- coding:utf-8 -*-
'''
Created on 27.04.2018 г.

@author: dedal
'''
#-*- coding:utf-8 -*-

import ConfigParser
import os

class ConfWarning(ConfigParser.Error):
    pass

class ConfError(ConfigParser.Error):
    pass


class ConfFile():
    '''
    Работа с конфигурационен файл!

    Функции:
        __init__(confName)
        update_option(section, **option)
        add_section(section)
        add_option(section, **option)
        get(section, option = None, return_type = None)
        add_comment(section, comment)

    '''

    def __init__(self, confName):
        '''
        :param confName: /home/my_user/my.conf
        '''
        self.parser = ConfigParser.ConfigParser(allow_no_value=True)

        var = open(confName, 'r')  # @UnusedVariable
        self.file_name = confName

    def update_option(self, section, **option):
        '''
            Change option in config file
            :param section: section name
            :param option: oprion_1 = True, option_2 = 'data', option_3 = 1.2
            :return: True
        USE:
            conf.update_option('system', version='1.1.0')
        '''
        self.parser.read(self.file_name)
        for i in option.keys():
            var = self.parser.has_option(section, i)
            if var == True:
                self.parser.set(section, i, option[i])
                myfile = open(self.file_name, 'w')
                self.parser.write(myfile)
                myfile.close()
                return True
            else:
                raise ConfError, 'Option %s not exist!' % (i)

    def add_section(self, section):
        '''
        ADD NEW Section:
            :param section: log_level
            :return: True
        
        use:
            conf.add_section('udp')
        '''
        self.parser.read(self.file_name)
        var = self.parser.has_section(section)
        if var == False:
            self.parser.add_section(section)
            myfile = open(self.file_name, 'w')
            self.parser.write(myfile)
            myfile.close()
            return True
        else:
            raise ConfWarning, 'Section %s already exist!' % (section)

    def add_option(self, section, **option):
        '''
        Add Option in section:
            :param section: section name
            :param option:  option_1 = 'one', option_2 = 2
            :return: True

        USE:
            conf.add_option('system', iv_jump = True)
        '''
        try:
            self.parser.read(self.file_name)
            for i in option.keys():
                var = self.parser.has_option(section, i)
                if var == False:
                    self.parser.set(section, i, option[i])
                    myfile = open(self.file_name, 'w')

                else:
                    raise ConfWarning, 'Option %s already exist!' % (i)
        except UnboundLocalError:
            raise ConfError, 'Section %s is mising.' % (section)
        self.parser.write(myfile)
        myfile.close()
        return True

    def get(self, section, option=None, return_type=None):
        '''
            Get all section
                :param section: if get all section
                :return: {'option_1':'1', 'option_2':'True'}
            Get single option from section
                :param section: section name
                :param option: option name
                :param return_type: None, ini, float, bool, str
                :return: option value

            return_type get option:
                None, ini, float, bool, str
                defolt is str
            USE:
                conf.get('udp', 'port', 'int')
        '''
        self.parser.read(self.file_name)
        if option == None and return_type == None:
            var = self.parser.items(section)
            res = {}
            for i in var:
                res[i[0]] = i[1]
            return res
        elif option == None and return_type != None:
            raise ConfError, 'Not have selected option!'
        elif option != None and return_type == None:
            return self.parser.get(section, option)
        elif option != None and return_type == 'str':
            return self.parser.get(section, option)
        elif option != None and return_type == 'int':
            return self.parser.getint(section, option)
        elif option != None and return_type == 'float':
            return self.parser.getfloat(section, option)
        elif option != None and return_type == 'bool':
            return self.parser.getboolean(section, option)
        elif (return_type != 'int' or return_type != 'float' or return_type != 'bool'
              or return_type != None):
            raise ConfError, ('''Invalid return_type %s.
            Type must be (int, bool, float or None)''' % (return_type))

    def add_comment(self, section, comment):
        '''
            Add coment in .conf file
                :param section: section name
                :param comment: comment on 1 row

            USE:
                conf.add_comment('udp', 'timeout is udp socket timeout')
                conf.add_option('udp', timeout=10)
                
        '''
        self.parser.read(self.file_name)
        self.parser.set(section, ';%s' % (comment))
        myfile = open(self.file_name, 'w')
        self.parser.write(myfile)
        myfile.close()
        return True