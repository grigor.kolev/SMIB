#-*- coding:utf-8 -*-
'''
Created on 27.04.2018 г.

@author: dedal
'''
import serial
import datetime

POLL_EXCEPTION = {
    '00': 'No activity',
    '01': 'No Response',
    '11': 'Slot door was opened',
    '12': 'Slot door was closed',
    '13': 'Drop door was opened',
    '14': 'Drop door was closed',
    '15': 'Card cage was opened',
    '16': 'Card cage was closed',
    '17': 'AC power was applied to gaming machine',
    '18': 'AC power was lost from gaming machine',
    '19': 'Cashbox door was opened',
    '1a': 'Cashbox door was closed',
    '1b': 'Cashbox was removed',
    '1c': 'Cashbox was installed',
    '1d': 'Belly door was opened',
    '1e': 'Belly door was closed',
    '1f': 'No activity and waiting for player input (obsolete)',
    '20': 'General tilt (Use this tilt when other exception tilt codes do not apply or '
          'when the tilt condition cannot be determined.)',
    '21': 'Coin in tilt',
    '22': 'Coin out tilt',
    '23': 'Hopper empty detected',
    '24': 'Extra coin paid',
    '25': 'Diverter malfunction (controls coins to drop or hopper)',
    '27': 'Cashbox full detected',
    '28': 'Bill jam',
    '29': 'Bill acceptor hardware failure',
    '2a': 'Reverse bill detected',
    '2b': 'Bill rejected',
    '2c': 'Counterfeit bill detected',
    '2d': 'Reverse coin in detected',
    '2e': 'Cashbox near full detected',
    '31': 'CMOS RAM error (data recovered from EEPROM)',
    '32': 'CMOS RAM error (no data recovered from EEPROM)',
    '33': 'CMOS RAM error (bad device)',
    '34': 'EEPROM error (data error)',
    '35': 'EEPROM error (bad device)',
    '36': 'EPROM error (different checksum – version changed)',
    '37': 'EPROM error (bad checksum compare)',
    '38': 'Partitioned EPROM error (checksum – version changed)',
    '39': 'Partitioned EPROM error (bad checksum compare)',
    '3a': 'Memory error reset (operator used self test switch)',
    '3b': 'Low backup battery detected',
    '3c': 'Operator changed options (This is sent whenever the operator changes'
          'configuration options. This includes, but is not limited to, denomination,'
          'gaming machine address, or any option that affects the response to long polls'
          '1F, 53, 54, 56, A0, B2, B3, B4, or B5.)',
    '3d': 'A cash out ticket has been printed',
    '3e': 'A handpay has been validated',
    '3f': 'Validation ID not configured',
    '40': 'Reel Tilt (Which reel is not specified.)',
    '41': 'Reel 1 tilt',
    '42': 'Reel 2 tilt',
    '43': 'Reel 3 tilt',
    '44': 'Reel 4 tilt',
    '45': 'Reel 5 tilt',
    '46': 'Reel mechanism disconnected',
    '47': '$1.00 bill accepted (non-RTE only)',
    '48': '$5.00 bill accepted (non-RTE only)',
    '49': '$10.00 bill accepted (non-RTE only)',
    '4a': '$20.00 bill accepted (non-RTE only)',
    '4b': '$50.00 bill accepted (non-RTE only)',
    '4c': '$100.00 bill accepted (non-RTE only)',
    '4d': '$2.00 bill accepted (non-RTE only)',
    '4e': '$500.00 bill accepted (non-RTE only)',
    '4f': 'Bill accepted (In non-RTE mode, use this exception for all bills without a specific exception. In RTE mode, use for all bill denominations.)',
    '50': '$200.00 bill accepted (non-RTE only)',
    '51': 'Handpay is pending (Progressive, non-progressive or cancelled credits)',
    '52': 'Handpay was reset (Jackpot reset switch activated)',
    '53': 'No progressive information has been received for 5 seconds',
    '54': 'Progressive win (cashout device/credit paid)',
    '55': 'Player has cancelled the handpay request',
    '56': 'SAS progressive level hit',
    '57': 'System validation request',
    '60': 'Printer communication error',
    '61': 'Printer paper out error',
    '66': 'Cash out button pressed',
    '67': 'Ticket has been inserted',
    '68': 'Ticket transfer complete',
    '69': 'AFT transfer complete',
    '6a': 'AFT request for host cashout',
    '6b': 'AFT request for host to cash out win',
    '6c': 'AFT request to register',
    '6d': 'AFT registration acknowledged',
    '6e': 'AFT registration cancelled',
    '6f': 'Game locked',
    '70': 'Exception buffer overflow',
    '71': 'Change lamp on',
    '72': 'Change lamp off',
    '74': 'Printer paper low',
    '75': 'Printer power off',
    '76': 'Printer power on',
    '77': 'Replace printer ribbon',
    '78': 'Printer carriage jammed',
    '79': 'Coin in lockout malfunction (coin accepted while coin mech disabled)',
    '7a': 'Gaming machine soft (lifetime-to-date) meters reset to zero',
    '7b': 'Bill validator (period) totals have been reset by an attendant/operator',
    '7c': 'A legacy bonus pay awarded and/or a multiplied jackpot occurred',
    '7e': 'Game has started',
    '7f': 'Game has ended',
    '80': 'Hopper full detected',
    '81': 'Hopper level low detected',
    '82': 'Display meters or attendant menu has been entered',
    '83': 'Display meters or attendant menu has been exited',
    '84': 'Self test or operator menu has been entered',
    '85': 'Self test or operator menu has been exited',
    '86': 'Gaming machine is out of service (by attendant)',
    '87': 'Player has requested draw cards (only send when in RTE mode)',
    '88': 'Reel N has stopped (only send when in RTE mode)',
    '89': 'Coin/credit wagered (only send when in RTE mode, and only send if the'
          'configured max bet is 10 or less)',
    '8a': 'Game recall entry has been displayed',
    '8b': 'Card held/not held (only send when in RTE mode)',
    '8c': 'Game selected',
    '8e': 'Component list changed',
    '8f': 'Authentication complete',
    '98': 'Power off card cage access',
    '99': 'Power off slot door access',
    '9a': 'Power off cashbox door access',
    '9b': 'Power off drop door access'
}

# Допустими кокманди за един брояч. Използва се от command.get_single_meter()
SINGLE_METHER_COMMAND = {
    'curent credit': {'command':'1A','size':8, 'denom': True}, # Връша текущия кредит във float спрямо коефицента
    'out': {'command':'10','size':8, 'denom': True}, # Връща out във float спрямо коефицент
    'out credit': {'command':'10','size':8, 'denom': False}, # Връща out в int като брояч
    'true out': {'command':'2B','size':8, 'denom': True}, # Връща out във float спрямо коефицент
    'true out credit': {'command':'2B','size':8, 'denom': False}, # Връща out в int като брояч
    'bill': {'command':'20','size':8, 'denom': False}, # Връща bill като int
    'bet': {'command':'11','size':8, 'denom': True}, # Връща bet във float спрямо коефицента
    'won': {'command':'12','size':8, 'denom': True}, # Връща won като float спрямо коефицент
    'in': {'command':'13','size':8, 'denom': True}, # Връща in във float спрямо коефицент
    'in credit': {'command':'13','size':8, 'denom': False}, # Връща in в int като брояч
    'true in': {'command':'2A','size':8, 'denom': True}, # Връща in във float спрямо коефицент
    'true in credit': {'command':'2A','size':8, 'denom': False}, # Връща in в int като брояч
    'jp': {'command':'14','size':8, 'denom': True}, # Връща jp във float спрямо коефицент
    'game played': {'command':'15','size':8, 'denom': False}, # Връща брой изиграни игри в int
    'game won': {'command':'16','size':8, 'denom': False}, # Връща брой спечелени игри в int
    'game lost': {'command':'17','size':8, 'denom': False}, # Връща брой загубени игри в int
    'game implement': {'command':'51','size':6, 'denom': False}, # Връща броя на игрите в машината в int
    'selected game': {'command': '55', 'size':6, 'denom':False}, # Връща номера на текущо избраната игра в int
    'denomination': {'command': 'B3', 'size':5, 'denom':False}, # Връша текущо избраната деноминация в инт
    'halt': {'command': '01', 'size': 0, 'denom':False},
    'start': {'command': '02', 'size': 0, 'denom':False},
    'halt bill': {'command': '07', 'size': 0, 'denom':False},
    'start bill': {'command': '06', 'size': 0, 'denom':False},
    'halt autoplay': {'command': 'AA00', 'size': 0, 'denom':False},
    'start autoplay': {'command': 'AA01', 'size': 0, 'denom':False},
}


# Допустими команти за повече от един брояч използва се от функция command.get_multi_meter()
MULTI_METHER_COMMAND = {
    '19': {'command': '19', 'size': 24, 'field': [
        {'name': 'bet', 'size': 4, 'denom': True},
        {'name': 'won', 'size':4, 'denom': True},
        {'name': 'in', 'size':4, 'denom':True},
        {'name': 'jp', 'size': 4, 'denom': True},
        {'name': 'games played', 'size': 4, 'denom': False},
        ]},
    '0F': {'command': '0F', 'size': 28, 'field': [
        {'name': 'out', 'size': 4, 'denom': True},
        {'name': 'bet', 'size': 4, 'denom': True},
        {'name': 'won', 'size': 4, 'denom': True},
        {'name': 'in', 'size': 4, 'denom': True},
        {'name': 'jp', 'size': 4, 'denom': True},
        {'name': 'games played', 'size': 4, 'denom': False},
        ]},
    '1C': {'command': '1C', 'size': 36, 'field': [
        {'name': 'bet', 'size':4, 'denom':True},
        {'name': 'won', 'size':4, 'denom':True},
        {'name': 'in', 'size':4, 'denom':True},
        {'name': 'jp', 'size':4, 'denom':True},
        {'name': 'games played', 'size': 4, 'denom': False},
        {'name': 'game won', 'size': 4, 'denom': False},
        {'name': 'door opened', 'size': 4, 'denom': False},
        {'name': 'power reset', 'size': 4, 'denom': False},
    ]},
    '19 credit': {'command': '19', 'size': 24, 'field': [
        {'name': 'bet', 'size': 4, 'denom': False},
        {'name': 'won', 'size':4, 'denom': False},
        {'name': 'in', 'size':4, 'denom':False},
        {'name': 'jp', 'size': 4, 'denom': False},
        {'name': 'games played', 'size': 4, 'denom': False},
        ]},
    '0F credit': {'command': '0F', 'size': 28, 'field': [
        {'name': 'out', 'size': 4, 'denom': False},
        {'name': 'bet', 'size': 4, 'denom': False},
        {'name': 'won', 'size': 4, 'denom': False},
        {'name': 'in', 'size': 4, 'denom': False},
        {'name': 'jp', 'size': 4, 'denom': False},
        {'name': 'games played', 'size': 4, 'denom': False},
        ]},
    '1C credit': {'command': '1C', 'size': 36, 'field': [
        {'name': 'bet', 'size':4, 'denom':False},
        {'name': 'won', 'size':4, 'denom':False},
        {'name': 'in', 'size':4, 'denom':False},
        {'name': 'jp', 'size':4, 'denom':False},
        {'name': 'games played', 'size': 4, 'denom': False},
        {'name': 'game won', 'size': 4, 'denom': False},
        {'name': 'door opened', 'size': 4, 'denom': False},
        {'name': 'power reset', 'size': 4, 'denom': False},
    ]},
    'bill': {'command': '1E', 'size': 28, 'field': [
        {'name': '1', 'size': 4, 'denom': False},
        {'name': '5', 'size': 4, 'denom': False},
        {'name': '10', 'size': 4, 'denom': False},
        {'name': '20', 'size': 4, 'denom': False},
        {'name': '50', 'size': 4, 'denom': False},
        {'name': '100', 'size': 4, 'denom': False},
    ]},
    'meter for game': {'command': '52', 'size': 22, 'field':[
        {'name':'Game number', 'size': 2, 'denom': False},
        {'name': 'bet', 'size': 4, 'denom':True},
        {'name': 'won', 'size':4, 'denom':True},
        {'name': 'jp', 'size':4, 'denom':True},
        {'name':'games played', 'size':4, 'denom':False}
    ]},
    'game conf': {'command': '53', 'size':26, 'field':[
        {'name': 'game number', 'size': 2, 'denom': False },
        {'name': 'game id', 'size':2, 'denom':None},
        {'name': 'Additional ID', 'size':3, 'denom':None},
        {'name':'Denomination', 'size': 1, 'denom':None},
        {'name': 'Max bet', 'size':1, 'denom': None},
        {'name': 'Progressive group', 'size':1, 'denom': None},
        {'name': 'Game options', 'size': 2, 'denom': None},
        {'name': 'Paytable', 'size': 6, 'denom': None},
        {'name': 'Base %', 'size': 4, 'denom': None},
    ]},
    'bonus win': {'command': '90', 'size':14, 'field':[
        {'name': 'multiplier', 'size': 1, 'denom': False },
        {'name': 'multiplied win', 'size': 4, 'denom': True },
        {'name': 'tax', 'size': 1, 'denom': False },
        {'name': 'bonus', 'size': 4, 'denom': True },
    ]},
    'legacy bonus': {'command': '9A', 'size':18, 'field':[
        {'name': 'game bumber', 'size': 2, 'denom': False },
        {'name': 'deductible', 'size': 4, 'denom': True },
        {'name': 'non-deductible', 'size': 4, 'denom': True },
        {'name': 'wager match', 'size': 4, 'denom': True },
    ]},
}


# Възможни коефиценти. Използва се от command.coef()
DENOMINATION = {
    '00': None,
    '01': 0.01,
    '17': 0.02,
    '02': 0.05,
    '03': 0.10,
    '04': 0.25,
    '05': 0.50,
    '06': 1.00,
    '07': 5.00,
    '08': 10.00,
    '09': 20.00,
}

    
class BadCRC(Exception):
    pass

class SASUSBReset(Exception):
    pass

class NoSasConnection(Exception):
    pass

class NoSasDenom(Exception):
    pass

class SASOpenError(Exception):
    pass

class SASMashinNone(Exception):
    pass

class EMGNotResponse(Exception):
    pass

class SASPortIsOpen(Exception):
    pass

class Sas():
    def __init__(self, port, speed, timeout=None, usbreset_tag=None):
        self.ser = serial.Serial()
        self.ser.port = port
        self.ser.baudrate = speed
        self.ser.timeout = timeout
        self.usbreset_tag = usbreset_tag
        self.mashin_n = None
        self.denom = None
    
    def flushInput(self):
        return self.ser.flushInput()
        
    def flushOutput(self):
        return self.ser.flushOutput()
        
    def crc(self, response, chk=False, seed=0):
        '''
        Изчислява crc на SAS протокола!
        :param response: Инфо от машината при chk = True, Команда при chk=False
        :param chk: при True проверява чек сума. При False генерира чек сума
        :param seed: винаги 0
        :return: При chk=True връща True или генерира грешка BadCRC. При chk=False връща crc на подадена информация.
        '''
        c = ''
        if chk != False:
            crc = response[-4:]
            response = response[:-4]
    
        for x in response:
            c = c+x
            if len(c) == 2:
                q = (seed ^ int(c, 16)) & 0o17
                seed = (seed >> 4) ^ (q * 0o010201)
                q = (seed ^ (int(c, 16) >> 4)) & 0o17
                seed = (seed >> 4) ^ (q * 0o010201)
                c=''
        data = hex(seed)
        if len(data) == 5:
            data = data[0:2] + '0' + data[2:]
        elif len(data) == 4:
            data = data[0:2] + '00' + data[2:]
        elif len(data) == 3:
            data = data[0:2] + '000' + data[2:]
        elif len(data) == 2:
            data = data[0:2] + '0000'
        if chk == False:
            data = data[4:] + data[2:-2]
#            pass
        else:
            data = data[4:] + data[2:-2]
            if data == crc:
                return True
            else:
                raise BadCRC, response
        return data
    
    def isOpen(self):
        return self.ser.isOpen()
    
    def usbreset(self):
        if self.usbreset_tag != None:
            try:
                import usbreset
            except ImportError as e:
                raise SASUSBReset, e
            else:
                usbreset.usb_reset(self.usbreset_tag)
    
    def close(self):
        return self.ser.close()
    
    def open(self):
        try:
            if self.isOpen() == True:
                self.close()
                raise SASPortIsOpen
            return self.ser.open()
        except serial.SerialException as e:
            raise SASOpenError, e
        
class LPoll(Sas):
    def __init__(self, port, speed, timeout=None, usbreset_tag=None):
        Sas.__init__(self, port, speed, timeout, usbreset_tag)
        
    def l_poll_conf(self):
        self.ser.parity = serial.PARITY_MARK
        self.ser.stopbits = serial.STOPBITS_ONE
        
    def get_mashin_n(self):
        self.l_poll_conf()
        self.open()
        self.flushOutput()
        self.flushInput()
        self.mashin_n = self.ser.read(1).encode('hex')
        self.close()
        if self.mashin_n == '':
            self.mashin_n = None
            raise NoSasConnection
        else:
            return True
        return False
        
    def command(self, cmd, size):
        self.l_poll_conf()
        self.open()
        try:
            self.ser.write(('82' + self.mashin_n).decode('hex'))
        except TypeError as e:
            self.close()
            raise SASMashinNone, e
        self.ser.parity = serial.PARITY_SPACE
        crc = self.crc(self.mashin_n + cmd)
        cmd = cmd + crc
        self.ser.write(cmd.decode('hex'))
        response = self.ser.read(size).encode('hex')
        self.close()
        if response == '':
            return False
        self.crc(response, chk=True)
        return response[4:-4]
    
    def coef(self, **kwargs):
        response = self.command('1F', 24)
        if response != False:
            try:
                self.denom = DENOMINATION[response[10:12]]
            except KeyError as e:
                self.denom = None
                raise NoSasDenom, e
            return DENOMINATION[response[10:12]]
        return None
    
    def get_date_time(self):
        response = self.command('7E', 11)
        if response != False:
            return datetime.datetime.strptime(response, '%m%d%Y%H%M%S')
        else:
            return None
    
    def sas_version(self):
        response = self.command('5400', 20)
        if response == False:
            return None
        else:
            return int(response[3:6])* 0.01
        return None
    
    def delay_game(self, delay_time=100):
        delay_time = str(delay_time)
        cmd = '2E' + ('0' * (4-len(delay_time))+delay_time)
        self.command(cmd, 0)
        return True
    
    def set_date_time(self, dates, times):
        cmd = '7F' + dates.replace('.', '') + times.replace(':', '') + '00'
        self.command(cmd, 0)
        return self.get_date_time()
    
    def get_multi_meter(self, command, game_n=None):
        command = MULTI_METHER_COMMAND[command]
        cmd = command['command']
        if game_n != None:
            cmd = cmd + game_n
        response = self.command(cmd, command['size'])
        if response == False:
            return None
        var = {}
        count = 0
        if command['size'] == 0:
            return True
        for item in command['field']:
            if item['denom'] == False:
                var[item['name']] = int(response[count:count+ (item['size']*2)])
            elif item['denom'] == None:
                var[item['name']] = response[count:count + (item['size'] * 2)]
            else:
                var[item['name']] = round(int(response[count:count + (item['size'] * 2)])*self.denom, 2)
            count = count + (item['size']*2)
        return var

    
    def get_single_meter(self, command):
        command = SINGLE_METHER_COMMAND[command]
        cmd = command['command']
        response = self.command(cmd, command['size'])
        if response != False:
            if command['denom'] is True:
                return round(int(response)*self.denom, 2)
            else:
                return int(response)
        elif command['size'] == 0:
            return True
        else:
            return None
    
    def set_legacy_bonus(self, mony, tax='00'):
        if self.denom > 0.01:
            return None
        game_selected = self.get_single_meter('selected game')
        if game_selected == 0 or game_selected == None:
            return None
#         credit = self.get_single_meter('curent credit')
#         if credit == None or credit == 0:
#             return None
        cmd = str(int((mony/self.denom)))
        cmd = cmd.replace('.', '')
        cmd = '0' * (8-len(cmd)) + cmd
        cmd = '8A' + cmd + tax
        self.command(cmd, 0)
        return True
    
    def multi_cmd(self, **kwargs):
        var = {}
        if 'bill' in kwargs and kwargs['bill'] == True:
            var['bill'] = self.get_single_meter(command='bill')
        if 'credit' in kwargs and kwargs['credit'] == True:
            var['credit'] = self.get_single_meter(command='curent credit')
        if 'credit_0F' in kwargs  and kwargs['credit_0F'] == True:
            var['credit_0F'] = self.get_multi_meter(command='0F credit')
        if 'mony_0F' in kwargs  and kwargs['mony_0F'] == True:
            var['mony_0F'] = self.get_multi_meter(command='0F')
        if 'coef' in kwargs:
            var['coef'] = self.denom
        for i in kwargs:
            if kwargs[i] == None:
                return None
        else:
            return var
    
    def get_mether_with_len(self, **kwargs):
        pass

class GPoll(Sas):

    def __init__(self, port, speed, timeout=None, usbreset_tag=None):
        Sas.__init__(self, port, speed, timeout, usbreset_tag)
        self.g_poll_conf()

    def g_poll_conf(self):
        self.ser.patity = serial.PARITY_NONE
        self.ser.stopbits = serial.STOPBITS_TWO

    def get_event(self):
        self.open()
#         self.flushOutput()
#         self.flushInput()
        cmd = '8281'
        self.ser.write(cmd.decode('hex'))
        response = self.ser.read(1).encode('hex')
        self.close()
        if response == '':
            return None
        try:
            response = POLL_EXCEPTION[response]
        except KeyError:
            return None
        return response
    
class ModLPoll(LPoll):
    
    def command(self, cmd, size):
        self.l_poll_conf()
        try:
            crc = self.crc(self.mashin_n + cmd)
            cmd = cmd + crc
        except TypeError as e:
            raise SASMashinNone, e
        self.open()
#         self.flushOutput()
#         self.flushInput()
        self.ser.write(('82'+self.mashin_n).decode('hex'))
        self.close()
        self.ser.parity = serial.PARITY_SPACE
        self.open()
        self.ser.write(cmd.decode('hex'))
        response = self.ser.read(size).encode('hex')
        self.close()
        if response == '':
            return False
        self.crc(response, chk=True)
        return response[4:-4]


class AmaticSerialSas(ModLPoll):
    
    def l_poll_conf(self):
        self.ser.parity = serial.PARITY_NONE
        self.ser.stopbits = serial.STOPBITS_ONE
        
    def command(self, cmd, size):
        self.l_poll_conf()
        try:
            crc = self.crc(self.mashin_n + cmd)
            cmd = cmd + crc
        except TypeError as e:
            raise SASMashinNone, e
        self.open()
        self.flushOutput()
        self.flushInput()
        self.ser.write(('82' + self.mashin_n).decode('hex'))
        self.ser.parity = serial.PARITY_SPACE
        self.ser.write(cmd.decode('hex'))
        response = self.ser.read(size).encode('hex')
        self.close()
        if response == '':
            return False
        self.crc(response, chk=True)
        return response[4:-4]
    
class AmaticUsbSas(LPoll):
    
    def l_poll_conf(self):
        self.ser.parity = serial.PARITY_NONE
        self.ser.stopbits = serial.STOPBITS_ONE
    
    def command(self, cmd, size):
        self.l_poll_conf()
        try:
            crc = self.crc( self.mashin_n + cmd)
            cmd=cmd+crc
        except TypeError as e:
            self.close()
            raise SASMashinNone, e
        self.open()
        self.flushOutput()
        self.flushInput()
        try:
            self.ser.write((self.mashin_n+cmd).decode('hex'))
        except TypeError as e:
            self.close()
            raise SASMashinNone, e
        response = self.ser.read(size).encode('hex')
        self.close()
        if response == '':
            return False
        self.crc(response, chk=True)
        return response[4:-4]
    
if __name__ == '__main__':
    sas = LPoll( port='/dev/ia', speed=19200, timeout=1)
    sas.mashin_n = '01'
    sas.denom = 0.01
    for i in range(3):
        print sas.get_multi_meter(command='0F')