#-*- coding:utf-8 -*-
'''
Created on 27.04.2018 г.

@author: dedal
'''
import sys
    
class NoSerial(Exception):
    pass

class BadSerialVersion(Exception):
    pass
import serial
# try:
#     import serial
# except ImportError as e:
#     raise NoSerial, e
# if serial.VERSION != '3.2.1':
#     raise BadSerialVersion, '3.2.1 required'

class RFIDReadError(serial.SerialException):
    pass

class RFIDNoUSBReset(Exception):
    pass

class RFIDOpenError(Exception):
    pass

class RFIDCommandError(Exception):
    pass

class RFIDWriteError(Exception):
    pass

class RFID():
    
    def __init__(self, port, baudrate, timeout=False, usb_reset_tag=None):

        self.ser = serial.Serial()
        self.ser.baudrate = baudrate
        self.ser.port = port
        if timeout != False:
            self.ser.timeout = timeout
        self.usb_reset_tag = usb_reset_tag
        
    def open(self):
        try:
            self.ser.open()
        except serial.serialutil.SerialException as e:
            raise RFIDOpenError, e
        
    def read(self, size):
        try:
            self.flushInput()
        except Exception as e:
            raise RFIDReadError, e
        else:
            data = self.ser.read(size)
            return data
    
    def write(self, block, value):
        try:
            self.flushOutput()
        except Exception as e:
            raise RFIDWriteError, e
        command = 'ew' + str(block) + ',' + str(value) + '\r\n'
        for i in range(5):  # @UnusedVariable
            self.ser.write(command)
            self.read(40)
        return True
    
    def run_command(self, command):
#         self.ser.flushInput()
        try:
            self.flushOutput()
            command = command + '\r\n'
            for i in range(5):  # @UnusedVariable
                self.ser.write(command)
                self.read(20)
        except Exception as e:
            raise RFIDCommandError, e
        return True
    
    def _read_block(self, from_block, to_block=None):
        if to_block == None:
            command = 'er' + str(from_block) + '\r\n'
        else:
            command = 'er' + str(from_block) + ',' + str(to_block) + '\r\n'
        for i in range(5):  # @UnusedVariable
            self.ser.write(command)
            self.read(20)
        return True
    
    def get_id(self):
        data = self.read(12)
        if data[3:-1] == '':
            return False
        return data[3:-1]
    
    def conf_block(self, block):
        if block != None:
            self._read_block(block)
#         self.close()
            
    def get_block(self):
        data = self.read(79)
        if data[3:-1] == '':
            return False
        data =  data[27:-5]
        var = ''
        for i in data:
            if i.encode('hex') != '20':
                var = var+i
        return var
    
    def close(self):
        self.ser.close()
        
    def isOpen(self):
        return self.ser.isOpen()
    
    def flushInput(self):
        self.ser.flushInput()
        return True
        
    def flushOutput(self):
        self.ser.flushOutput()
        return True
    
    def usbreset(self):
        if self.usb_reset_tag != None:
            try:
                import usbreset
            except ImportError as e:
                raise RFIDNoUSBReset, e
            else:
                usbreset.usb_reset(self.usb_reset_tag)
        
if __name__ == '__main__':
    cart = RFID('/dev/ttyACM0', 15200, timeout=0)
    cart.open()
    while True:
        print cart.get_id()