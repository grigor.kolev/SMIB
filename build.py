#-*- coding:utf-8 -*-
'''
Created on 19.03.2018 г.

@author: dedal
'''
import os
# from bbfreeze import Freezer

def get_files(directory):
    file_paths = []
    for root, directories, files in os.walk(directory):  
        for filename in files:
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)
    return file_paths
    
def build(path, user, group='adm'):
    files = get_files(path)
    for i in files:
        if i[-4:] == '.pyc':
            cmd = 'rm %s' % (i)
            os.system(cmd)
            print 'remove %s' % (i)
            print 'DONE'
        elif i[len(path):len(path)+6] == '.fuse_':
            pass
        elif  i[-3:] == '.so':
            pass
        elif  i[-5:] == '.html': 
            pass
        elif  i[-4:] == '.pdf':
            pass
        elif  i[-4:] == '.png':
            pass
        elif  i[-4:] == '.ico':
            pass
        elif i[-4:] == '.img':
            pass
        elif i[-4:] == '.wav':
            pass
        elif i[-4:] =='.fbp':
            pass
        elif i[-3:] == '.po':
            pass
        elif i[-3:] == '.mo':
            pass
        elif i[-6:] == 'run.py':
            pass
        elif i[-9:] == 'guirun.py':
            pass
        
        elif i[-11:] != '__init__.py' and i[-7:] != 'main.py' and i[-8:] != 'main.pyw':
            cmd = 'cython %s -o %s.c' % (i, i[0:-3])
            print 'make %s to C extention' % (i)
            os.system(cmd)
            cmd = 'rm %s' % (i)
            os.system(cmd)
            print 'DONE'
            cmd = 'gcc -shared -pthread -fPIC -fwrapv  -Wall -fno-strict-aliasing -I/usr/include/python2.7 -o %s.so %s.c' % (i[0:-3], i[0:-3])
            print 'Make %s.c to SO extention' % (i[0:-3])
            os.system(cmd)
            cmd = 'rm %s.c' % (i[0:-3])
            os.system(cmd)
            print 'DONE'
    os.system('chown -R %s:%s %s' % (user, group, path))
    print 'FINISH!'
   
def pyinstaller_gui(path, passwd):
    os.chdir(path)
    cmd = "pyinstaller main.py -n GuiConf --key passwd -w --icon GuiConf.ico"
    os.system(cmd)
    

if __name__ == '__main__':
    a = raw_input('Gui Build (N/Y): ')
    path = raw_input('SOFT TO BUILD: ')
    if a == 'Y':
        passwd = raw_input('PASSWD: ')
        pyinstaller_gui(path, passwd)
    else:
        user =  raw_input('FOLDER USER: ')
        build(path, user)

        